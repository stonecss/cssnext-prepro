<?php

/**
 * Process files using 'cssnext'.
 */
function _cssnext_prepro_compile($file) {
  $cssnextCLI = new cssnextPhpCli($file);

  $dependencies[] = $file;

  // SUITCSS CLI does not offer to show dependencies for a given file, so
  // revert to default mechanism
  _custom_prepro_find_dependencies($file, $dependencies);
  _custom_prepro_cache_dependencies($file, $dependencies);

  return array(
    $cssnextCLI->compile(),
    $cssnextCLI->get_error(),
  );
}
